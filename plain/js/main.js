

angular.module("solver", [])
	.controller("mainController", mainController)
	.filter('makeRange', makeRange);
;


function makeRange()
{
	return function(input) 
	{
		var lowBound, highBound;
		switch (input.length)
		{
		case 1:
			lowBound = 0;
			highBound = parseInt(input[0]) - 1;
			break;
		case 2:
			lowBound = parseInt(input[0]);
			highBound = parseInt(input[1]);
			break;
		default:
			return input;
		}
		var result = [];
		for (var i = lowBound; i <= highBound; i++)
		{
			result.push(i);
		}
		return result;
	};
}


function mainController($scope, $timeout)
{
	var scopeDebounce = function(fn, time)
	{
		return _.debounce(function()
		{
			$scope.$apply(fn);
		}, time);
	};
	$scope.editMode = true;
	
	$scope.size = 
	{
		width: 5,
		height: 5
	};
	
	$scope.constraints = 
	{
		rows: [],
		cols: []
	};
	
	$scope.$watch("size.width", scopeDebounce(function()
	{
		adjustConstraintContainer($scope.constraints.cols, $scope.size.width || 0);
	}, 250));

	$scope.$watch("size.height", scopeDebounce(function()
	{
		adjustConstraintContainer($scope.constraints.rows, $scope.size.height || 0);
	}, 250));
	
	$scope.addConstraint = function(constraints)
	{
		constraints.push(1);
	};
	
	$scope.removeConstraint = function(constraints, index)
	{
		constraints.splice(index, 1);
	};
	
	$scope.export = function()
	{
		$scope.importExport = JSON.stringify($scope.constraints, null, 2)
	};
	
	$scope.import = function()
	{
		var newConstraints = JSON.parse($scope.importExport);
		$scope.size.width = newConstraints.cols.length;
		$scope.size.height = newConstraints.rows.length;
		
		$scope.constraints = newConstraints;
	};
	
	$scope.solve = function()
	{
		$scope.editMode = false;
		
		$scope.plainConstraints = 
		{
			cols: buildPlainConstraints($scope.constraints.cols),
			rows: buildPlainConstraints($scope.constraints.rows)
		};
		
		var field = [];
		for(var row = 0; row < $scope.size.height; row++)
		{
			var rowData = [];
			for(var col = 0; col < $scope.size.width; col++)
			{
				rowData.push(null);
			}	
			field.push(rowData);
		}

		var step = {type: "initial", field: field};
		$scope.steps = [step];
		
		$scope.selectStep(step);
		
		$timeout(makeStep.bind(null, $scope.steps, $scope.constraints, field, $timeout), 25);
	};
		
	$scope.selectStep = function(step)
	{
		$scope.currentField = step.field;
	};
		
	// BEispiel. Sp�ter weg
	$scope.importExport = '{\n"rows": [\n[1,1],\n[1,3],\n[5],\n[1,3],[1]\n],\n"cols": [\n[3],\n[1,1,1],\n[3],\n[4],\n[3]\n]\n}\n';
	
}


function makeStep(steps, constraints, field, $timeout) 
{
	if(!isSolved(field, constraints))
	{
		var step = findDefiniteStep(field, constraints);
		if(step)
		{
			steps.push(step);
			$timeout(makeStep.bind(null, steps, constraints, step.field, $timeout), 25);
		}
		else
		{
			// TODO: make guess
			console.log("No step found");
		}
	}
}

function findDefiniteStep(field, constraints)
{
	var result = findEasyDefiniteStep(field, constraints);
	// TODO: add more complicated steps
	return result;
}

function isSolved()
{
	// TODO: implement
	return false;
}


function findEasyDefiniteStep(field, constraints)
{
	// rows
	for(var i = 0; i < constraints.rows.length; i++)
	{
		var rowConstraints = constraints.rows[i];
		var rowField = extractRow(field, i);
		var result = findEasyDefiniteStepInLine(rowField, rowConstraints);
		if(result)
		{
			if(result.length === rowField.length)
			{
				return {
					type: "easy",
					orientation: "row",
					index: i,
					field: setRow(field, i, result)
				};
			}
			else
			{
				console.error("Result size is wrong");
				return null;
			}
		}
	}
	
	// rows
	for(i = 0; i < constraints.cols.length; i++)
	{
		var colConstraints = constraints.cols[i];
		var colField = extractCol(field, i);
		var result = findEasyDefiniteStepInLine(colField, colConstraints);
		if(result)
		{
			if(result.length === colField.length)
			{
				return {
					type: "easy",
					orientation: "col",
					index: i,
					field: setCol(field, i, result)
				};
			}
			else
			{
				console.error("Result size is wrong");
				return null;
			}
		}
	}
	
}

function setRow(field, index, row)
{
	var result = _.cloneDeep(field);
	for(var i = 0; i < row.length; i++)
	{
		result[index][i] = row[i];
	}
	return result;
}


function setCol(field, index, col)
{
	var result = _.cloneDeep(field);
	for(var i = 0; i < col.length; i++)
	{
		result[i][index] = col[i];
	}
	return result;
}

function isSet(element)
{
	return element === true;
}

function isFree(element)
{
	return element === false;
}

function isUnspecified(element)
{
	return _.isNull(element);
}

function findEasyDefiniteStepInLine(line, constraints)
{
	console.log(line);
	console.log(constraints);
	
	var resultingLine = [];
	var changed = false; // did a change happen or was the line already solved?
	
	var constraintIndex = -1;
	var usedFromCurrentConstraint = 0;
	for(var index = 0; index < line.length; index++)
	{
		var currentElement = line[index];
		// before first constraint
		if(constraintIndex === -1)
		{
			if(isFree(currentElement))
			{
				resultingLine.push(currentElement);
				continue;
			}
			else
			{
				constraintIndex++; // first constraint begins
			}
		}
		
		if(constraintIndex < constraints.length)
		{
			var currentConstraint = constraints[constraintIndex];
			// constraint needs more
			if(usedFromCurrentConstraint < currentConstraint)
			{
				if(isSet(currentElement))
				{
					resultingLine.push(currentElement);
					usedFromCurrentConstraint++;
				}
				else if(isUnspecified(currentElement))
				{
					resultingLine.push(true);
					usedFromCurrentConstraint++;
					changed = true;
				}
				else
				{
					// free
					// beginning of constraint? multiple frees are allowed
					if(usedFromCurrentConstraint === 0)
					{
						resultingLine.push(currentElement);
					}
					else
					{
						// Space in constraint: error
						return null;	
					}
				}
			}
			else
			{
				// constraint is full, need space
				if(isSet(currentElement))
				{
					return null;
				}
				else
				{
					resultingLine.push(false);
					usedFromCurrentConstraint = 0;
					constraintIndex++;
					if(isUnspecified(currentElement))
					{
						changed = true;
					}
				}	
			}
		}
		else
		{
			if(isFree(currentElement))
			{
				resultingLine.push(currentElement);
			}
			else
			{
				return null;
			}
		}
	}
	
	if(changed)
	{
		if(constraintIndex < constraints.length)
		{
			var currentConstraint = constraints[constraintIndex];
			if(usedFromCurrentConstraint < currentConstraint)
			{
				return null;
			}
		}

		return resultingLine;	
	}
	else
	{
		return null;
	}
}

function extractRow(field, index)
{
	return field[index];
}

function extractCol(field, index)
{
	var result = [];
	for(var row = 0; row < field.length; ++row)
	{
		result.push(field[row][index]);
	}
	
	return result;
}

function buildPlainConstraints(constraints)
{
	var maxSize = 0;
	var result = _.map(constraints, function(constraintLine)
	{
		var result = _.filter(constraintLine, function(constraint) 
		{
			return _.isNumber(constraint) && constraint > 0;
		});
		if(result.length > maxSize)
		{
			maxSize = result.length;
		}
		return result;
	});

	_.each(result, function(constraintLine) 
	{
		while(constraintLine.length < maxSize)
		{
			constraintLine.splice(0, 0, null);
		}
	});
	
	return result;
}


function adjustConstraintContainer(container, newSize)
{
	if(newSize > container.length)
	{
		for(var i = container.length; i <= newSize; i++)
		{
			container.push([]);
		}
	}
	else if(newSize < container.length)
	{
		// TODO: ist irgendwie zu gro�....
		container.splice(newSize);
	}
}





